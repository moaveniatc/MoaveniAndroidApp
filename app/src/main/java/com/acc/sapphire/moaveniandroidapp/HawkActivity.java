package com.acc.sapphire.moaveniandroidapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

public class HawkActivity extends AppCompatActivity implements View.OnClickListener {

    TextView txtViewTitle;
    TextView txtViewResult;
    EditText editTxtTel;
    EditText editTxtEmail;
    Button btnSave;
    Button btnShow;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hawk);
        Hawk.init(this).build();
        bindViews();
        setPageTitle();
    }

    private void bindViews() {
        txtViewTitle = (TextView) findViewById(R.id.txt_view_title);
        txtViewResult = (TextView) findViewById(R.id.txt_view_result);
        editTxtTel = (EditText) findViewById(R.id.edit_txt_tel);
        editTxtEmail = (EditText) findViewById(R.id.edit_txt_email);
        btnSave = (Button) findViewById(R.id.btn_save);
        btnShow = (Button) findViewById(R.id.btn_show);
        btnSave.setOnClickListener(this);
        btnShow.setOnClickListener(this);
    }

    private void setPageTitle() {
        Intent hawkIntent = getIntent();
        txtViewTitle.setText(hawkIntent.getStringExtra("title"));
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == btnSave.getId()) {
            setPref("tel", editTxtTel.getText().toString());
            setPref("email", editTxtEmail.getText().toString());
            Toast.makeText(this, "Saved successfully.", Toast.LENGTH_SHORT).show();
            txtViewResult.setText("");
        } else if (view.getId() == btnShow.getId()) {
            String res = "Your Tel no: '" + getPref("tel") + "'\n"
                    + "Your Email: '" + getPref("email") + "'";
            txtViewResult.setText(res);
        }

    }

    private void setPref(String key, String value) {
        Hawk.put(key, value);
    }

    private String getPref(String key) {
        return Hawk.get(key);
    }
}
