package com.acc.sapphire.moaveniandroidapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.TextView;

import com.acc.sapphire.moaveniandroidapp.adapters.TvAdapter;
import com.acc.sapphire.moaveniandroidapp.models.TvModel;

import java.util.ArrayList;
import java.util.List;

public class GridViewActivity extends AppCompatActivity {
    GridView gridViewTv;
    List<TvModel> tvModels;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view);

        bindViews();
        //setPageTitle();
        prepareGridData();
        TvAdapter adapter = new TvAdapter(this,tvModels);
        System.out.println("Adapter created. . .");
        gridViewTv.setAdapter(adapter);
        System.out.println("Adapter Set. . .");

    }

    private void bindViews() {
        gridViewTv = (GridView) findViewById(R.id.gird_view_tv);
    }

    private void setPageTitle() {
        ((TextView) findViewById(R.id.txt_view_title)).setText(getIntent().getStringExtra("title"));
    }

    private void prepareGridData() {
        tvModels = new ArrayList<TvModel>();
        /*tvModels.add(new TvModel("شبکه یک","http://bt.static.telewebion.com/web/content_images/channel_images/thumbs/new/240/v4/tv1.png"));
        tvModels.add(new TvModel("شبکه دو","http://bt.static.telewebion.com/web/content_images/channel_images/thumbs/new/240/v4/tv2.png"));
        tvModels.add(new TvModel("شبکه سه","http://bt.static.telewebion.com/web/content_images/channel_images/thumbs/new/240/v4/tv3.png"));
        tvModels.add(new TvModel("شبکه چهار","http://bt.static.telewebion.com/web/content_images/channel_images/thumbs/new/240/v4/tv4.png"));
        tvModels.add(new TvModel("شبکه تهران","http://bt.static.telewebion.com/web/content_images/channel_images/thumbs/new/240/v4/tehran.png"));
        tvModels.add(new TvModel("شبکه خبر","http://bt.static.telewebion.com/web/content_images/channel_images/thumbs/new/240/v4/irinn.png"));
        tvModels.add(new TvModel("شبکه نسیم","http://bt.static.telewebion.com/web/content_images/channel_images/thumbs/new/240/v4/nasim.png"));
        tvModels.add(new TvModel("شبکه ورزش","http://bt.static.telewebion.com/web/content_images/channel_images/thumbs/new/240/v4/varzesh.png"));
        tvModels.add(new TvModel("شبکه پویا","http://bt.static.telewebion.com/web/content_images/channel_images/thumbs/new/240/v4/pooya.png"));*/

        tvModels.add(new TvModel("شبکه یک",R.drawable.tv1));
        tvModels.add(new TvModel("شبکه دو",R.drawable.tv2));
        tvModels.add(new TvModel("شبکه سه",R.drawable.tv3));
        tvModels.add(new TvModel("شبکه چهار",R.drawable.tv4));
        tvModels.add(new TvModel("شبکه تهران",R.drawable.tehran));
        tvModels.add(new TvModel("شبکه خبر",R.drawable.irinn));
        tvModels.add(new TvModel("شبکه نسیم",R.drawable.nasim));
        tvModels.add(new TvModel("شبکه ورزش",R.drawable.varzesh));
        tvModels.add(new TvModel("شبکه پویا",R.drawable.pooya));
    }

}
