package com.acc.sapphire.moaveniandroidapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class WebServiceActivity extends AppCompatActivity implements View.OnClickListener {
    TextView txtViewTitle;
    EditText editTxtTitle;
    EditText editTxtYear;
    Button btnShow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_service);
        bindViews();
        setPageTitle();

    }

    private void bindViews() {
        txtViewTitle = (TextView) findViewById(R.id.txt_view_title);
        editTxtTitle = (EditText) findViewById(R.id.edit_txt_title);
        editTxtYear = (EditText) findViewById(R.id.edit_txt_year);
        btnShow = (Button) findViewById(R.id.btn_show);
        btnShow.setOnClickListener(this);
    }

    private void setPageTitle() {
        Intent intent = getIntent();
        txtViewTitle.setText(intent.getStringExtra("title"));


    }

    @Override
    public void onClick(View view) {
        if (view.getId() == btnShow.getId()) {
            String movTitle = editTxtTitle.getText().toString();
            String movYear = editTxtYear.getText().toString();
            getDataFromOMDB(movTitle, movYear);   // Getting data from OMDB(Online Movie DataBase : http://www.omdbapi.com/)
        }
    }

    // Getting data from OMDB(Online Movie DataBase : http://www.omdbapi.com/)
    private String getDataFromOMDB(final String movTitle, final String movYear) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String url = "https://www.omdbapi.com/?t=" + movTitle + "&y=" + movYear;
                //String url = "http://services.groupkt.com/country/get/iso2code/IN";
                System.out.println("******* URL: " + url + "**************");
                try {
                    URL u = new URL(url);
                    HttpURLConnection con = (HttpURLConnection) u.openConnection();
                    con.setRequestMethod("GET");
                    if (con.getResponseCode() != 200) {
                        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                        String InputLine;
                        StringBuffer response = new StringBuffer();
                        while ((InputLine = in.readLine()) != null) {
                            response.append(InputLine);
                        }
                        parsJson(response.toString());

                        Log.d("rest_api", response.toString());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }).start();
        return "";
    }

    void parsJson(String omdbResponse) {
        try {
            JSONObject rootObj = new JSONObject(omdbResponse);
            String titleStr = rootObj.getString("Title");
            String yearStr = rootObj.getString("Year");
            String releasedStr = rootObj.getString("Released");
            String runtimeStr = rootObj.getString("Runtime");
            String genreStr = rootObj.getString("Genre");
            String directorStr = rootObj.getString("Director");
            String writerStr = rootObj.getString("Writer");
            String actorsStr = rootObj.getString("Actors");
            String plotStr = rootObj.getString("Plot");
            String langStr = rootObj.getString("Language");
            String countryStr = rootObj.getString("Country");
            String posterStr = rootObj.getString("Poster");
            System.out.println("Title: " + titleStr);
            System.out.println("Year: " + yearStr);
            System.out.println("Released: " + releasedStr);
            System.out.println("Runtime: " + runtimeStr);
            System.out.println("Genre: " + genreStr);
            System.out.println("Director: " + directorStr);
            System.out.println("Writer: " + writerStr);
            System.out.println("Actors: " + actorsStr);
            System.out.println("Plot: " + plotStr);
            System.out.println("Language: " + langStr);
            System.out.println("Contry: " + countryStr);
            System.out.println("Poster: " + posterStr);
            /*JSONObject rootObj = new JSONObject(omdbResponse);
            String resResponseStr= rootObj.getString("RestResponse");

            JSONObject resResponseObj = new JSONObject(resResponseStr);
            String resultStr = resResponseObj.getString("result");

            JSONObject resultObj = new JSONObject("resultStr");
            String str0 = resultObj.getString("0");

            JSONObject obj0 = new JSONObject(str0);
            String name = obj0.getString("name");
            String code2 = obj0.getString("alpha2_code");
            String code3 = obj0.getString("alpha3_code");
            System.out.println("Name: " + name);
            System.out.println("Code2: " + code2);
            System.out.println("Code3: " + code3);*/


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
