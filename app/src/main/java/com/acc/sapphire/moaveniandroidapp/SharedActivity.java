package com.acc.sapphire.moaveniandroidapp;

import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SharedActivity extends AppCompatActivity implements View.OnClickListener {
    TextView txtViewTitle;
    TextView txtViewResult;
    EditText editTxtName;
    EditText editTxtFamily;
    Button btnSave;
    Button btnShow;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared);
        bindViews();
        setPageTitle();
    }

    private void bindViews() {
        txtViewTitle = (TextView) findViewById(R.id.txt_view_title);
        txtViewResult = (TextView) findViewById(R.id.txt_view_result);
        editTxtName = (EditText) findViewById(R.id.edit_txt_name);
        editTxtFamily = (EditText) findViewById(R.id.edit_txt_family);
        btnSave = (Button) findViewById(R.id.btn_save);
        btnShow = (Button) findViewById(R.id.btn_show);
        btnSave.setOnClickListener(this);
        btnShow.setOnClickListener(this);
    }

    private void setPageTitle() {
        Intent SharedIntent = getIntent();
        txtViewTitle.setText(SharedIntent.getStringExtra("title"));
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == btnSave.getId()) {
            setPref("name", editTxtName.getText().toString());
            setPref("family", editTxtFamily.getText().toString());
            Toast.makeText(this, "Saved successfully.", Toast.LENGTH_SHORT).show();
        } else if (view.getId() == btnShow.getId()) {
            String res = "'" + getPref("name","noName") + " "
                    + getPref("family","noFamily") + "'";
            res += " is saved in 'Shared Preference'.";
            txtViewResult.setText(res);
        }

    }

    private void setPref(String key, String value) {
        PreferenceManager.getDefaultSharedPreferences(this).edit().putString(key, value).apply();
    }

    private String getPref(String key,String defaultValue) {
        return PreferenceManager.getDefaultSharedPreferences(this).getString(key,"");
    }
}
