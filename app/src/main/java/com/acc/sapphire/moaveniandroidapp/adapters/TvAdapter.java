package com.acc.sapphire.moaveniandroidapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.acc.sapphire.moaveniandroidapp.R;
import com.acc.sapphire.moaveniandroidapp.models.TvModel;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Sapphire on 10/31/2017.
 */

public class TvAdapter extends BaseAdapter {
    Context mContext;
    List<TvModel> tvModels;

    public TvAdapter(Context mContext, List<TvModel> tvModels) {
        this.mContext = mContext;
        this.tvModels = tvModels;
    }

    @Override
    public int getCount() {
        return tvModels.size();
    }

    @Override
    public Object getItem(int position) {
        return tvModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        //View gridView = LayoutInflater.from(mContext).inflate(R.layout.tv_grid_item, viewGroup, true);
        View gridView = LayoutInflater.from(mContext).inflate(R.layout.tv_grid_item, viewGroup, false);
        ImageView tvLogo = gridView.findViewById(R.id.empty_image_view);
        TextView tvName = gridView.findViewById(R.id.empty_txt_view);

        Picasso.with(mContext).load(tvModels.get(position).getImage()).into(tvLogo);
        //tvLogo.setImageResource(tvModels.get(position).getImage());

        tvName.setText(tvModels.get(position).getName());
        return null;
    }
}
