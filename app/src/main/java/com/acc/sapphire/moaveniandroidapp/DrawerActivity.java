package com.acc.sapphire.moaveniandroidapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DrawerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);
        Intent drawerIntent = getIntent();
        ((TextView)findViewById(R.id.txt_view_title)).setText(drawerIntent.getStringExtra("title"));
    }
}
