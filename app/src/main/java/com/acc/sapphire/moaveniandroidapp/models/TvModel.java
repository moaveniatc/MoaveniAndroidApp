package com.acc.sapphire.moaveniandroidapp.models;

import android.media.Image;

/**
 * Created by Sapphire on 10/31/2017.
 */

public class TvModel {
    String name;
    int image;
    //String image;

    public TvModel(String name, int image) {
        this.name = name;
        this.image = image;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public int getImage() {
        return image;
    }
}
