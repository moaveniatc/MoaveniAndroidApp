package com.acc.sapphire.moaveniandroidapp;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnShared;
    Button btnHawk;
    Button btnDrawer;
    Button btnGrid;
    Button btnWebService;
    Button btnGSON;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindViews();
    }

    public void bindViews() {
        btnShared = (Button) findViewById(R.id.btn_shared);
        btnHawk = (Button) findViewById(R.id.btn_hawk);
        btnDrawer = (Button) findViewById(R.id.btn_drawer);
        btnGrid = (Button) findViewById(R.id.btn_grid);
        btnWebService = (Button) findViewById(R.id.btn_web_service);
        btnGSON = (Button) findViewById(R.id.btn_GSON);

        btnShared.setOnClickListener(this);
        btnHawk.setOnClickListener(this);
        btnDrawer.setOnClickListener(this);
        btnGrid.setOnClickListener(this);
        btnWebService.setOnClickListener(this);
        btnGSON.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == btnShared.getId()) {
            //Toast.makeText(this, "Shared Pref", Toast.LENGTH_SHORT).show();
            Intent sharedIntent = new Intent(MainActivity.this, SharedActivity.class);
            sharedIntent.putExtra("title", "Shared Preference Activity");
            startActivity(sharedIntent);
        } else if (view.getId() == btnHawk.getId()) {
            Intent hawkIntent = new Intent(MainActivity.this, HawkActivity.class);
            hawkIntent.putExtra("title", "Hawk Activity");
            startActivity(hawkIntent);
        } else if (view.getId() == btnDrawer.getId()) {
            Intent drawerIntent = new Intent(MainActivity.this, DrawerActivity.class);
            drawerIntent.putExtra("title", "Navigation Drawer Activity");
            startActivity(drawerIntent);
        } else if (view.getId() == btnGrid.getId()) {
            Intent gridIntent = new Intent(MainActivity.this, GridViewActivity.class);
            gridIntent.putExtra("title", "Gird View Activity");
            startActivity(gridIntent);
        } else if (view.getId() == btnWebService.getId()) {
            Intent webIntent = new Intent(MainActivity.this, WebServiceActivity.class);
            webIntent.putExtra("title", "Web Service Activity");
            startActivity(webIntent);
        } /*else if(view.getId() == btnGSON.getId()) {
            Intent gsonIntent = new Intent(MainActivity.this, GridViewActivity.class);
            gridIntent.putExtra("title", "Gird View Activity");
            startActivity(gridIntent);
        }*/
    }
}
